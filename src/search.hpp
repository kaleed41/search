#include <iostream>
#include <string>
#include <vector>

using namespace std;

int search(string text, string pattern)
{
	for(int i = 0; i < text.size(); i++)
	{
		bool result = true;

		for(int k = 0; k < pattern.size(); k++)
		{

			if(text[i+k] != pattern[k])
			{
				result = false;
				break;
			}
		}

		if(pattern.size() == 0)
		{
			pattern = "";
			return -1
		}

		if (result == true)
		{
			return 1;
		}
	}

	return -1;
}
